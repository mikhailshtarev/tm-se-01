package org.shtarev.tmse02;

import org.shtarev.tmse02.entyty.Project;
import org.shtarev.tmse02.entyty.Task;
import org.shtarev.tmse02.repository.ProjectRepository;
import org.shtarev.tmse02.repository.TaskRepository;
import org.shtarev.tmse02.service.ServiceProject;
import org.shtarev.tmse02.service.ServiceTask;

import java.util.Scanner;

public class Bootstrap {
    private final TaskRepository taskRepository = new TaskRepository();
    private final ServiceTask serviceTask = new ServiceTask(taskRepository);
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ServiceProject serviceProject=new ServiceProject(projectRepository);
    private final Scanner in = new Scanner(System.in);

    public void createTask(Scanner create) {
        System.out.println("Введите название задачи:");
        String name = create.next();
        System.out.println("Введите описание задачи:");
        String description = create.next();
        serviceTask.create(name, description);
    }

    public void readTask(Scanner read) {
        System.out.println("Введите название задачи:");
        String taskname = read.next();
        Task task = serviceTask.read(taskname);
        System.out.println("Наименование задачи" + task.getName());
        System.out.println("Описание задачи"+ task.getDescription());
    }

    public void updateTask(Scanner read) {
        System.out.println("Введите название задачи:");
        String taskName = read.next();
        Task task = serviceTask.update(taskName);
        System.out.println("Введите новое описание задачи:");
        String description = read.next();
        task.setDescription(description);
    }

    public void deleteTask(Scanner read) {
        System.out.println("Введите название задачи которую хотите удалить:");
        String taskName = read.next();
        Task task = serviceTask.delete(taskName);
    }
    public void createProject(Scanner read) {
        System.out.println("Введите название проекта: ");
        String name = read.next();
        System.out.println("Введите уровень приоритета проекта: ");
        String priority = read.next();
        System.out.println("Укажите менеджера, ведущего проект: ");
        String manager = read.next();
        serviceProject.create(name,priority,manager);
    }
    public void readProject(Scanner read) {
        System.out.println("Введите название проекта, который хотите посмотреть: ");
        String name = read.next();
        Project project = serviceProject.read(name);
        System.out.println("Название проекта: " + project.getNameProject());
        System.out.println("Приоритет проекта проекта: " + project.getPriorityProject());
        System.out.println("Менеджер проекта: " + project.getManagerProject());
    }
    public void updateProject(Scanner read) {
        System.out.println("Введите название проекта, содержание которого хотите изменить:");
        String name= read.next();
        Project project = serviceProject.update(name);
        System.out.println("Введите приоритет проекта:");
        String prior = read.next();
        project.setPriorityProject(prior);
        System.out.println("Введите менеджера, ведущего проект: ");
        String manager = read.next();
        project.setManagerProject(manager);
    }
    public void deleteProject(Scanner read) {
        System.out.println("Введите название проекта который хотите удалить:");
        String name = read.next();
        Project project = serviceProject.delete(name);
    }

    public void start() {
        while (true) {
            System.out.println("Help:");
            System.out.println ("1 :Сreate Task");
            System.out.println ("2 :Read Task");
            System.out.println( "3 :Update Task");
            System.out.println ("4 :Delete Task");
            System.out.println("5 :Create Project");
            System.out.println("6 :Read Project");
            System.out.println ("7 :Update Project");
            System.out.println("8 :Delete Project");
            System.out.print("Введите терминальную команду: ");
            String innum = in.next();
            switch (innum) {
                case "1":
                    createTask(in);
                    break;
                case "2":
                    readTask(in);
                    break;
               case "3":
                    updateTask(in);
                    break;
                case "4":
                    deleteTask(in);
                    break;
                case "5":
                    createProject(in);
                    break;
                case "6":
                    readProject(in);
                    break;
                case "7":
                    updateProject(in);
                    break;
                case "8":
                    deleteProject(in);
                    break;

            }
        }
    }
}

