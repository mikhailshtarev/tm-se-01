package org.shtarev.tmse02.service;

import org.shtarev.tmse02.entyty.Task;
import org.shtarev.tmse02.repository.TaskRepository;

public class ServiceTask {
    private final TaskRepository taskRepository;

    public ServiceTask (TaskRepository taskRepository) {
        this.taskRepository=taskRepository;
    }

    public void create(String name, String description) {
        taskRepository.create(name,description);

    }
    public Task read (String name) {
         return taskRepository.read(name);


    }
    public Task update(String name) {

        return taskRepository.update(name);
    }
    public Task delete (String name) {
        return taskRepository.delete(name);

    }


}
