package org.shtarev.tmse02.service;

import org.shtarev.tmse02.entyty.Project;
import org.shtarev.tmse02.repository.ProjectRepository;
import org.shtarev.tmse02.repository.TaskRepository;

public class ServiceProject {
    private final ProjectRepository projectRepository;

    public ServiceProject (ProjectRepository projectRepository) {
        this.projectRepository=projectRepository;
    }
    public void create (String name, String priority, String manager) {
        projectRepository.create(name,priority,manager);
    }
    public Project read (String name) {
        return projectRepository.read(name);
    }
    public Project update (String name) {
        return projectRepository.update(name);
    }
    public Project delete (String name) {
        return projectRepository.delete(name);
    }

}
