package org.shtarev.tmse02.entyty;

public class Task {
    String name;
    String description;

    public void setName(String name) {
        this.name=name;
    }
    public void setDescription(String description) {
        this.description=description;
    }
    public String getName() {
        return name;
    }
    public String getDescription() {
        return description;
    }
}
