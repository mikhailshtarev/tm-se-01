package org.shtarev.tmse02.entyty;

public class Project {
    String name;
    String priority;
    String manager;

    public void setNameProject(String name) { this.name = name;}
    public void setPriorityProject(String priority) { this.priority = priority;}
    public void setManagerProject(String manager) { this.manager = manager;}
    public String getNameProject() {
        return name;
    }
    public String getPriorityProject() {
        return priority;
    }
    public String getManagerProject() {
        return manager;
    }
}
