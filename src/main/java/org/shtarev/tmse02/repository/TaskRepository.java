package org.shtarev.tmse02.repository;

import org.shtarev.tmse02.entyty.Task;

import java.util.ArrayList;

public class TaskRepository {
    private final ArrayList<Task> taskList = new ArrayList<>();


    public void create(String name, String description) {
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskList.add(task);
    }

    public Task read(String name) {

        for (int i = 0; i < taskList.size(); i++) {
            Task ob = taskList.get(i);
            if (ob.getName().equals(name)) return ob;
        }
        return null;
    }

    public Task update(String name) {
        for (int i = 0; i < taskList.size(); i++) {
            Task ob = taskList.get(i);
            if (ob.getName().equals(name)) return ob;
        }
        return null;

    }

    public Task delete(String name) {
        for (int i = 0; i < taskList.size(); i++) {
            Task ob = taskList.get(i);
            if (ob.getName().equals(name)) return taskList.remove(i);
        }
        return null;
    }
}

