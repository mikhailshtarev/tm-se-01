package org.shtarev.tmse02.repository;

import org.shtarev.tmse02.entyty.Project;

import java.util.ArrayList;

public class ProjectRepository {
    private final ArrayList<Project> projectList = new ArrayList<>();

    public void create (String name, String priority, String manager) {
        Project project = new Project();
        project.setNameProject(name);
        project.setPriorityProject(priority);
        project.setManagerProject(manager);
        projectList.add(project);
    }
    public Project read (String name) {
        for (int i=0; i<projectList.size();i++) {
            Project ob = projectList.get(i);
            if (ob.getNameProject().equals(name)) return ob;
        }
        return null;
    }
    public Project update (String name) {
        for (int i=0; i<projectList.size();i++) {
            Project ob = projectList.get(i);
            if (ob.getNameProject().equals(name)) return ob;
        }
        return null;
    }
    public Project delete (String name) {
        for (int i=0; i<projectList.size();i++) {
            Project ob = projectList.get(i);
            if (ob.getNameProject().equals(name)) return projectList.remove(i);
        }
        return null;
    }
}
